/** @type {import('tailwindcss').Config} */
import lineClampPlugin from "@tailwindcss/line-clamp";
import colors from 'tailwindcss/colors';

export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    colors: {
      primary_dark: '#0A0F0D',
      dark: '#0A0F0D',
      green: colors.green,
      red: colors.red,
      blue: colors.blue,
      gray: colors.gray,
      white: colors.white,
      black: colors.black
    },
  },
  plugins: [lineClampPlugin],
};
