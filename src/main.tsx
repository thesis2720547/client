import React from "react";
import ReactDOM from "react-dom/client";
// import App from "./App.tsx";
import "./index.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Root, { loader as rootLoader } from "./pages/Root";
import LoginPage from "./pages/LoginPage";
import ConversationListPage, {
  loader as conversationListLoader,
} from "./pages/ConversationListPage";
import ConversationPage, {
  loader as conversationLoader,
} from "./pages/ConversationPage";
import QuestionPage, { loader as questionLoader } from "./pages/QuestionPage";
import AuthRoot, {loader as authLoader} from "./pages/AuthRoot";
import RegisterPage from "./pages/RegisterPage";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    loader: rootLoader,
    children: [
      {
        path: "",
        loader: conversationListLoader,
        element: <ConversationListPage />,
      },
      {
        path: "conversation",
        loader: conversationListLoader,
        element: <ConversationListPage />,
      },
      {
        path: "conversation/:convoId",
        loader: conversationLoader,
        element: <ConversationPage />,
      },
      {
        path: "conversation/:convoId/question/:questionId",
        loader: questionLoader,
        element: <QuestionPage />,
      },
    ],
  },
  {
    path: "/auth",
    loader: authLoader,
    element: <AuthRoot />,
    children: [
      {
        path: "login",
        element: <LoginPage />,
      },
      {
        path: "register",
        element: <RegisterPage />,
      }
    ]
  },
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
