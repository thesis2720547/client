import React from "react";
import { getAuth } from "../api/auth";
import { Outlet, redirect } from "react-router-dom";

interface IAuthRootProps {}

export const loader = async () => {
  try {
    const res = await getAuth();
    if (res.data.message?.userId) {
      return redirect("/");
    }
  } catch (err) {
    console.error(err);
  }
  return null;
};

const AuthRoot: React.FunctionComponent<IAuthRootProps> = () => {
  return (
    <div className="h-screen bg-primary flex items-center justify-center">
        <div className="bg-white max-w-[90vw] p-3 rounded-lg">
        <Outlet/>
        </div>
    </div>
  );
};

export default AuthRoot;
