import React, { useState } from "react";
import { login } from "../api/auth";
import { useNavigate } from "react-router-dom";
import { primaryButtonStyle } from "../styles";

interface ILoginPageProps {}

const LoginPage: React.FunctionComponent<ILoginPageProps> = () => {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onSubmit = async (e: any) => {
    e.preventDefault();
    const res = await login({ email, password });
    if (res.status === 200) {
      navigate("/");
    }
  };

  return (
    <form
      className="max-w-[80vw] w-[200px] flex flex-col gap-2"
      onSubmit={onSubmit}
    >
      <label
        className="block text-gray-700 text-sm font-semibold"
        htmlFor="email"
      >
        Email
      </label>
      <input
        name="email"
        className="px-2 py-1 border border-solid rounded-md"
        placeholder="abc@example.com"
        type="email"
        onChange={(e) => {
          setEmail(e.target.value);
        }}
      />
      <label
        className="block text-gray-700 text-sm font-semibold"
        htmlFor="password"
      >
        Password
      </label>
      <input
        name="password"
        className="px-2 py-1 border border-solid rounded-md"
        type="password"
        onChange={(e) => {
          setPassword(e.target.value);
        }}
      />
      <input className={primaryButtonStyle} type="submit" value="LOGIN" />
      <small className="text-center">
        No account?{" "}
        <a
          className="underline hover:text-gray-500 cursor-pointer"
          onClick={() => navigate("/auth/register")}
        >
          Register
        </a>{" "}
        instead
      </small>
    </form>
  );
};

export default LoginPage;
