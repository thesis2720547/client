import React, { useEffect, useMemo, useState } from "react";
import VideoRecorder from "../components/VideoRecorder";
import { getMediaUrl, getQuestion, postAnswer, putAnswerContent } from "../api";
import { useLoaderData, useNavigate, useParams } from "react-router-dom";
import { buttonStyle } from "../styles";
import { parseSpeechProsody } from "../utils/humeResultParser";
import MessageModal from "../components/Modal";

interface IQuestionPageProps {}

interface IQuestion {
  id: string;
  categoryId: number;
  convoId: string;
  content: string;
  answer: string;
  videoObjectKey: string;
  framesObjectKey: string;
  videoAnalysisJSON: string;
  videoAnalysisSummary: string;
  audioObjectKey: string;
  audioAnalysisJSON: string;
  audioAnalysisSummary: string;
  isCompleted: boolean;
}

export const loader = async ({ params }: any) => {
  try {
    const question: IQuestion = (await getQuestion(params.questionId)).data
      .question;
    let videoSrc = "";
    if (question?.videoObjectKey) {
      videoSrc = (await getMediaUrl(question.videoObjectKey)).data.url;
    }
    console.log({ question, videoSrc });
    return { question, videoSrc };
  } catch (err) {
    console.error(err);
    return null;
  }
};

const QuestionPage: React.FunctionComponent<IQuestionPageProps> = () => {
  const { question: originalQuestion, videoSrc: originalVideoSrc } =
    useLoaderData() as {
      question: IQuestion;
      videoSrc: string;
    };
  
    const navigate = useNavigate();

  const [question, setQuestion] = useState(originalQuestion);
  const [videoSrc, setVideoSrc] = useState(originalVideoSrc);
  const [content, setContent] = useState(
    question.answer || parseSpeechProsody(question?.audioAnalysisJSON)?.text
  );
  const [loading, setLoading] = useState<boolean>(!!videoSrc && !question.audioAnalysisJSON);
  const [finished, setFinished] = useState(false);

  useEffect(() => {
    setLoading(!!videoSrc && !question.audioAnalysisJSON);
  }, [question, videoSrc]);

  // trigger rerender on params change
  const { questionId } = useParams();
  useEffect(() => {
    const refetch = async () => {
      setLoading(true);
      if (!questionId) return;
      const newQuestion: IQuestion = (await getQuestion(questionId)).data
        .question;
      let newVideoSrc = "";
      if (question?.videoObjectKey) {
        newVideoSrc = (await getMediaUrl(newQuestion.videoObjectKey)).data.url;
      }
      setQuestion(newQuestion);
      setVideoSrc(newVideoSrc);
      setContent(newQuestion.answer);
      setLoading(false);
    };
    refetch();
  }, [questionId]);

  const onSubmitVideo = async (mediaBlobUrl: string) => {
    try {
      setLoading(true);
      const blob = await fetch(mediaBlobUrl).then((r) => r.blob());
      const video = new File([blob], "video.mp4", { type: "video/mp4" });
      const formData = new FormData();
      formData.append("video", video);
      formData.append("questionId", question.id);
      const res = await postAnswer(question.id, formData);
      setQuestion(res.data.question);
      if (res.data.question?.videoObjectKey) {
        const newVideoSrc = (
          await getMediaUrl(res.data.question?.videoObjectKey)
        ).data.url;
        setVideoSrc(newVideoSrc);
      }
      setFinished(true);
      setTimeout(() => {
        navigate(-1);
      }, 3000)
    } catch (err) {
      console.error(err);
      setLoading(false);
    }
  };

  const [isSubmitContentLoading, setIsSubmitContentLoading] = useState(false);

  const onSubmitContent = async (e: any) => {
    e.preventDefault();
    setIsSubmitContentLoading(true);
    const res = await putAnswerContent(question.id, content);
    setQuestion(res.data.question);
    console.log(res);
    setIsSubmitContentLoading(false);
  };

  const audioAnalysisBlob = useMemo(() => {
    if (!question?.audioAnalysisJSON) return;
    const blob = new Blob([JSON.stringify(JSON.parse(question.audioAnalysisJSON), null, '\t')], {
      type: 'text/plain',
    });
    return URL.createObjectURL(blob);
  }, [question.audioAnalysisJSON]);

  const videoAnalysisBlob = useMemo(() => {
    if (!question?.videoAnalysisJSON) return;
    const blob = new Blob([JSON.stringify(JSON.parse(question.videoAnalysisJSON), null, '\t')], {
      type: 'text/plain',
    });
    return URL.createObjectURL(blob);
  }, [question.videoAnalysisJSON]);

  return (
    <>
      <div className="h-screen overflow-y-auto w-full flex flex-col p-4">
        {question && <div className="font-semibold">{question.content}</div>}
        {videoSrc ? (
          <div className="grid grid-cols-2 gap-2 my-4">
            <div className="border-[8px] border-solid border-white h-fit w-fit">
            <video src={videoSrc} controls autoPlay />
            </div>
            <div>
              {question.videoAnalysisJSON || question.audioAnalysisJSON ? (
                <>
                  <form className="mb-4" onSubmit={onSubmitContent}>
                    {question.isCompleted ? (
                      <blockquote className="p-3 border-solid border-[1px] border-gray-300 italic bg-white">
                        {content}
                      </blockquote>
                    ) : (
                      <>
                        <textarea
                          disabled={isSubmitContentLoading}
                          value={content}
                          onChange={(e) => setContent(e.target.value)}
                          className="bg-gray-100 w-full h-[300px]"
                        />
                        <input
                          disabled={
                            question.isCompleted || isSubmitContentLoading
                          }
                          className={buttonStyle}
                          type="submit"
                          value="SUBMIT"
                        />
                      </>
                    )}{" "}
                    {isSubmitContentLoading && <p>Loading...</p>}
                  </form>

                  <div>
                    <div className="flex gap-2">
                      <h2 className="font-semibold">Facial Expression</h2>
                      <a
                        className="underline text-primary hover:text-gray-400"
                        href={videoAnalysisBlob}
                        download={`${questionId}_video_analysis.json`}
                      >
                        Download Details
                      </a>
                    </div>
                    <div className="bg-gray-100 p-2">
                      {question.videoAnalysisSummary.split(";").map((item) => (
                        <p>{item}</p>
                      ))}
                    </div>
                  </div>
                  <div>
                    <div className="flex gap-2">
                      <h2 className="font-semibold">Speech Prosody</h2>
                      <a
                        className="underline text-primary hover:text-gray-400"
                        href={audioAnalysisBlob}
                        download={`${questionId}_audio_analysis.json`}
                      >
                        Download Details
                      </a>
                    </div>
                    <div className="bg-gray-100 p-2">
                      {question.audioAnalysisSummary}
                    </div>
                  </div>
                </>
              ) : (
                <div>Loading...</div>
              )}
            </div>
          </div>
        ) : (
          <>
            <VideoRecorder className="max-w-[500px]" onSubmitVideo={onSubmitVideo} timeLimit={25} />
          </>
        )}
      </div>
      <MessageModal show={loading}>
        {finished
          ? "Answer submitted successfully. Awaiting analysis..."
          : "Loading..."}
      </MessageModal>
    </>
  );
};

export default QuestionPage;
