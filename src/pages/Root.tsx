import * as React from "react";
import Sidebar from "../components/Sidebar";
import { getAuth } from "../api/auth";
import { redirect, Outlet } from "react-router-dom";
import { socketWrapper } from "../utils/socket";

interface IRootProps {}

export const loader = async () => {
  try {
    const res = await getAuth();
    if (!res.data.message?.userId) {
      return redirect("/login");
    }
    socketWrapper.initSocket(res.data.message.userId);
    localStorage.setItem("user", JSON.stringify(res.data.message));
    return null;
  } catch (err) {
    localStorage.removeItem("user");
    console.error(err);
    return redirect("/auth/login");
  }
};

const Root: React.FunctionComponent<IRootProps> = () => {
  return (
    <div className="h-screen w-screen flex justify-center overflow-y-auto relative bg-[#f7f7f7]">
      <Sidebar />
      <Outlet />
    </div>
  );
};

export default Root;
