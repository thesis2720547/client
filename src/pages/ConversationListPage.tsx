import React, { useState } from "react";
import { buttonStyle } from "../styles";
import { getAllConversations } from "../api";
import { useLoaderData, useNavigate } from "react-router-dom";
import ConversationModal from "../components/ConversationModal";

interface IConversationListPageProps {}

export const loader = async () => {
  try {
    const res = await getAllConversations();
    return res.data;
  } catch (err) {
    console.error(err);
    return null;
  }
};

const ConversationListPage: React.FunctionComponent<
  IConversationListPageProps
> = () => {
  const { conversations } = useLoaderData() as any;
  const navigate = useNavigate();

  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <div className="w-full h-full overflow-y-auto pb-10 flex flex-col py-2 px-10 gap-2">
        <div>
          <button onClick={() => setShowModal(true)} className={buttonStyle}>
            Create conversation
          </button>
        </div>
        <div className="w-fit flex flex-wrap gap-3">
          {conversations &&
            conversations.map((conversation: any) => (
              <div
                key={conversation.id}
                className="relative bg-white p-2 rounded-md shadow-lg w-[300px] min-h-[358px] flex flex-col gap-2"
              >
                <p className="truncate text-ellipsis whitespace-nowrap">
                  <p className="font-semibold">Company Name: </p>
                  {conversation.companyName}
                </p>
                {conversation.jobDescription? <>
                  <p className="line-clamp-3">
                  <p className="font-semibold">Company Description: </p>
                  {conversation.companyDescription}
                </p>
                <p className="line-clamp-3">
                  <p className="font-semibold">Job Description: </p>
                  {conversation.jobDescription}
                </p>
                <p className="line-clamp-3">
                  <p className="font-semibold">Resumé Summary: </p>
                  {conversation.resumeSummary}
                </p>
                <div className="flex items-center justify-center mt-3">
                  <button
                    className={buttonStyle}
                    onClick={() => {
                      navigate(`conversation/${conversation.id}`);
                    }}
                  >
                    START
                  </button>
                </div>
                </>: <div className="absolute w-full h-full top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 bg-white/75 flex items-center">
                  <div className="text-center flex justify-center w-full bg-black/50 py-4 font-semibold text-white shadow-lg">Conversation is being generated...</div>
                  </div>}
              </div>
            ))}

          <ConversationModal
            showModal={showModal}
            setShowModal={setShowModal}
          />
        </div>
      </div>
    </>
  );
};

export default ConversationListPage;
