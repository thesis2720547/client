import React, { useState } from "react";
import { register } from "../api/auth";
import { useNavigate } from "react-router-dom";
import { primaryButtonStyle } from "../styles";

interface IRegisterPageProps {}

const RegisterPage: React.FunctionComponent<IRegisterPageProps> = () => {
  const navigate = useNavigate();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onSubmit = async (e: any) => {
    e.preventDefault();
    const res = await register({ name, email, password });
    if (res.status === 200) {
      navigate("/");
    }
  };

  return (
    <form
      className="max-w-[80vw] w-[200px] flex flex-col gap-2"
      onSubmit={onSubmit}
    >
      <label
        className="block text-gray-700 text-sm font-semibold"
        htmlFor="name"
      >
        Name
      </label>
      <input
        name="name"
        className="px-2 py-1 border border-solid rounded-md"
        placeholder="Joe Doe"
        type="name"
        onChange={(e) => {
          setName(e.target.value);
        }}
      />
      <label
        className="block text-gray-700 text-sm font-semibold"
        htmlFor="email"
      >
        Email
      </label>
      <input
        name="email"
        className="px-2 py-1 border border-solid rounded-md"
        placeholder="abc@example.com"
        type="email"
        onChange={(e) => {
          setEmail(e.target.value);
        }}
      />
      <label
        className="block text-gray-700 text-sm font-semibold"
        htmlFor="password"
      >
        Password
      </label>
      <input
        name="password"
        className="px-2 py-1 border border-solid rounded-md"
        type="password"
        onChange={(e) => {
          setPassword(e.target.value);
        }}
      />
      <input className={primaryButtonStyle} type="submit" value="REGISTER" />
      <small className="text-center">
        <p>Already have an account?</p>
        <a
          className="underline hover:text-gray-500 cursor-pointer"
          onClick={() => navigate("/auth/login")}
        >
          Login
        </a>{" "}
        instead
      </small>
    </form>
  );
};

export default RegisterPage;
