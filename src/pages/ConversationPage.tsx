import React, { useCallback, useMemo, useState } from "react";
import {
  getAllQuestions,
  getConversation,
  getConversationEvaluation,
} from "../api";
import { useLoaderData, useNavigate } from "react-router-dom";
import { FaCheck } from "react-icons/fa6";
import { buttonStyle, primaryButtonStyle } from "../styles";
import { PDFViewer } from "@react-pdf/renderer";
import ResultPDF from "../components/ResultPDF";
import Modal from "../components/Modal";
import { PiFileMagnifyingGlass } from "react-icons/pi";

interface IConversationPageProps {}

interface IQuestion {
  id: string;
  categoryId: number;
  convoId: string;
  content: string;
  isCompleted: boolean;
}

interface IConversation {
  id: string;
  userId: string;
  evaluation: {
    problemSolving: string;
    candidateUnderstanding: string;
    candidateAttitude: string;
    recommendation: string;
  };
  companyName: string;
  companyDescription: string;
  jobDescription: string;
  jobDescriptionObjectKey: string;
  resumeSummary: string;
  resumeObjectKey: string;
  domainId: number;
  roleId: number;
  isCompleted: boolean;
}

export const loader = async ({ params }: any) => {
  try {
    const questions = (await getAllQuestions(params.convoId)).data.questions;
    const conversation = (await getConversation(params.convoId)).data
      .conversation;
    console.log({
      questions,
      conversation: {
        ...conversation,
        evaluation: JSON.parse(conversation.evaluation),
      },
    });
    return {
      questions,
      conversation: {
        ...conversation,
        evaluation: JSON.parse(conversation.evaluation),
      },
    };
  } catch (err) {
    console.error(err);
    return null;
  }
};

const ConversationPage: React.FunctionComponent<
  IConversationPageProps
> = () => {
  const { questions, conversation } = useLoaderData() as {
    questions: IQuestion[];
    conversation: IConversation;
  };
  const [isLoading, setIsLoading] = useState(false);
  const [showPDF, setShowPDF] = useState(false);
  const navigate = useNavigate();

  const onSubmit = useCallback(async () => {
    try {
      setIsLoading(true);
      await getConversationEvaluation(conversation.id);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.error(error);
    }
  }, [conversation.id]);

  const canSubmit = useMemo(() => {
    return !conversation.isCompleted && questions.every(question => question.isCompleted);
  }, [conversation.isCompleted, questions]);
  return (
    <>
      <div className="w-full h-screen overflow-y-auto flex flex-col py-4 px-12 gap-2">
        {conversation.evaluation && (
          <div className="flex justify-center gap-3 border border-solid py-2">
            <h1 className="text-center text-2xl font-bold">Result Available</h1>
            <button
              onClick={() => setShowPDF(true)}
              className={`${primaryButtonStyle} inline-flex items-center gap-2 text-sm !px-3 !py-1`}
            >
              <PiFileMagnifyingGlass />
              <span>Read</span>
            </button>
          </div>
        )}
        <div className="flex flex-col gap-4 my-4">
          <p>
            <span className="font-semibold">Company Name: </span>
            {conversation.companyName}
          </p>
          <p className="">
            <span className="font-semibold">Company Description: </span>
            {conversation.companyDescription}
          </p>
          <p>
            <span className="font-semibold">Job Description: </span>
            {conversation.jobDescription}
          </p>
          <p className="">
            <span className="font-semibold">Resumé Summary: </span>
            {conversation.resumeSummary}
          </p>
        </div>
        <div>
          <h1 className="text-center text-2xl font-bold mb-4">Questions</h1>
          {questions &&
            questions.map((question: IQuestion) => (
              <div
                key={question.id}
                onClick={() => {
                  navigate(`question/${question.id}`);
                }}
                className="bg-white shadow-lg hover:bg-gray-100 p-2 cursor-pointer flex justify-between items-center gap-10 pr-5"
              >
                <p className="truncate">
                  <span className="font-semibold">
                    Category {question.categoryId}:{" "}
                  </span>
                  {question.content}
                </p>

                <FaCheck
                  className={`${
                    question.isCompleted ? "opacity-1" : " opacity-0"
                  } text-2xl text-green-700 shrink-0`}
                />
              </div>
            ))}
          {canSubmit && (
            <div className="flex flex-col justify-center items-center my-5">
              <button
                onClick={onSubmit}
                className={`${buttonStyle} text-lg bg-white`}
              >
                Submit Conversation
              </button>
              {isLoading && <div>Loading...</div>}
            </div>
          )}
        </div>
      </div>
      <Modal className="w-[80vw] h-[80vh]" show={showPDF} setShow={setShowPDF}>
        <div className="pt-5">
          <PDFViewer style={{ width: "100%" }} height={400}>
            <ResultPDF
              evaluation={conversation.evaluation}
              conversationId={conversation.id}
            />
          </PDFViewer>
        </div>
      </Modal>
      <Modal
        className="max-w-[500px] h-full flex items-center justify-center"
        show={conversation.isCompleted && !conversation.evaluation}
      >
        <div className="h-full">
          Conversation is being analyzed. Please wait...
        </div>
      </Modal>
    </>
  );
};

export default ConversationPage;
