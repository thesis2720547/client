import { memo, useEffect, useRef, useState } from "react";
import { useReactMediaRecorder } from "react-media-recorder";
import { buttonStyle } from "../styles";

const VideoPreview = ({ stream }: { stream: MediaStream | null }) => {
  const videoRef = useRef<HTMLVideoElement>(null);
  const timestampRef = useRef<HTMLSpanElement>(null);

  useEffect(() => {
    const onTimeUpdate = () => {
      if (!timestampRef.current?.innerHTML) return;
      console.log(videoRef.current?.currentTime)
      timestampRef.current.innerHTML = videoRef.current?.currentTime.toString() as string;
    }
    const videoElement = document.getElementById("preview-video");
    if (!videoElement) return;
    videoElement.addEventListener('timeupdate', onTimeUpdate);
    return () => {
      videoElement.removeEventListener('timeupdate', onTimeUpdate);
    }
  }, [stream]);

  useEffect(() => {
    if (videoRef.current && stream) {
      videoRef.current.srcObject = stream;
    }
  }, [stream]);

  if (!stream) {
    return null;
  }

  return (
    <div className="relative">
      <video id="preview-video" ref={videoRef} autoPlay />
      <div className="absolute bottom-0 left-1/2 -translate-x-1/2">
        <svg className="w-[40px] h-[40px]">
          <circle fill="#ff0000" stroke="none" cx="20" cy="20" r="12">
            <animate
              attributeName="opacity"
              dur="1s"
              values="0;1;0"
              repeatCount="indefinite"
              begin="0.1"
            />
          </circle>
        </svg>
      </div>
    </div>
  );
};

const OutputVideo = memo(({ src }: { src?: string }) => {
  return <video controls src={src} autoPlay />;
});

const VideoRecorder = ({
  timeLimit = 5,
  onSubmitVideo,
  className = "",
}: {
  timeLimit?: number;
  onSubmitVideo: (mediaBlobUrl: string) => void;
  className?: string;
}) => {
  const { status, startRecording, stopRecording, mediaBlobUrl, previewStream } =
    useReactMediaRecorder({
      video: true,
      blobPropertyBag: {
        type: "video/mp4",
      },
    });

  const intervalRef = useRef<ReturnType<typeof setTimeout>>();
  const currentTimeRef = useRef(0);

  // stop recording after time limit
  // useEffect(() => {
  //   if (status === "recording") {
  //     clearTimeout(intervalRef.current);
  //     intervalRef.current = setInterval(() => {
  //       if (currentTimeRef.current === timeLimit) {
  //         stopRecording();
  //         clearInterval(intervalRef.current);
  //         return;
  //       }
  //       currentTimeRef.current = currentTimeRef.current + 1;
  //     }, 1000);
  //     return;
  //   }
  //   if (intervalRef.current) {
  //     clearInterval(intervalRef.current);
  //     intervalRef.current = undefined;
  //   }
  // }, [status, stopRecording, timeLimit]);

  return (
    <div className={`w-full flex flex-col gap-4 ${className}`}>
      <div>
        <button className={buttonStyle} onClick={startRecording}>
          Start
        </button>
        <button className={buttonStyle} onClick={stopRecording}>
          Stop
        </button>
      </div>
      {status === "recording" && (
        <>
          <VideoPreview stream={previewStream} />
        </>
      )}
      {mediaBlobUrl && (
        <>
          <OutputVideo src={mediaBlobUrl} />
          <button
            className={buttonStyle}
            onClick={() => onSubmitVideo(mediaBlobUrl)}
          >
            Submit
          </button>
        </>
      )}
    </div>
  );
};

export default VideoRecorder;
