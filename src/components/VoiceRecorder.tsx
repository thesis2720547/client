import React, { useEffect } from "react";
import { useAudioRecorder } from "react-audio-voice-recorder";
import { buttonStyle } from "../styles";

interface IVoiceRecorderProps {
  onRecordingBlob: (blob: Blob | undefined) => void;
  disabled?: boolean;
  timeLimit?: number;
}

const VoiceRecorder: React.FunctionComponent<IVoiceRecorderProps> = ({
  onRecordingBlob,
  disabled = false,
  timeLimit = 5,
}) => {
  const {
    startRecording,
    stopRecording,
    // togglePauseResume,
    recordingBlob,
    isRecording,
    // isPaused,
    recordingTime,
    // mediaRecorder,
  } = useAudioRecorder();

  useEffect(() => {
    if (recordingBlob) {
      onRecordingBlob(recordingBlob);
    }
  }, [onRecordingBlob, recordingBlob]);

  useEffect(() => {
    if (recordingTime > timeLimit) {
      stopRecording();
    }
  }, [recordingTime, stopRecording, timeLimit]);

  return (
    <div className="flex h-[40px]">
      <button
        className={`${buttonStyle}`}
        disabled={isRecording || disabled}
        onClick={startRecording}
      >
        Record
      </button>
      <button
        className={`${buttonStyle}`}
        disabled={!isRecording}
        onClick={stopRecording}
      >
        Stop
      </button>
      {timeLimit - recordingTime}
    </div>
  );
};

export default VoiceRecorder;
