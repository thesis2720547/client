import React, { useCallback, useEffect, useState } from "react";
import { postAudio, postChat } from "../api";
import { buttonStyle } from "../styles";
import { socket } from "../utils/socket";
import VoiceRecorder from "./VoiceRecorder";

interface IOldChatBoxProps {}

interface IMessage {
  role: string;
  content: string;
}

interface IEmotionData {
  emotions: { name: string; score: number }[];
  text: string;
}

const OldChatBox: React.FunctionComponent<IOldChatBoxProps> = () => {
  const [currentMessage, setCurrentMessage] = useState("");
  const [currentEmotions, setCurrentEmotions] = useState<string[]>([]);
  const [messages, setMessages] = useState<IMessage[]>([
    {
      role: "assistant",
      content:
        "Good morning, are you ready to start the interview? [neutral, 70%]",
    },
  ]);
  const [isAIThinking, setIsAIThinking] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [audioURL, setAudioURL] = useState("");

  useEffect(() => {
    function onConnect() {
      console.log("connect");
    }

    function onDisconnect() {
      console.log("disconnect");
    }

    const onResult = (data: IEmotionData) => {
      setCurrentEmotions(data.emotions.map((emotion) => emotion.name));
      setCurrentMessage(data.text);
      setIsLoading(false);
      setAudioURL("");
    };

    const onTest = () => {
      console.log("received test");
    };

    const onVideoResult = (data: any) => {
      console.log("video result", data);
    };

    socket.on("connect", onConnect);
    socket.on("disconnect", onDisconnect);
    socket.on("video-result", onVideoResult);
    socket.on("result", onResult);
    socket.on("test", onTest);
    return () => {
      socket.off("connect", onConnect);
      socket.off("disconnect", onDisconnect);
      socket.off("result", onResult);
      socket.off("video-result", onVideoResult);
      socket.off("test", onTest);
    };
  }, []);

  const handleSubmit = useCallback(async () => {
    setIsLoading(true);
    if (!audioURL) return;
    const blob = await fetch(audioURL).then((r) => r.blob());
    const file = new File([blob], "testAudio1.wav");
    const formData = new FormData();
    formData.append("file", file);
    const res = await postAudio(formData);
    console.log(res);
  }, [audioURL]);

  const postMessage = useCallback(async (_messages: IMessage[]) => {
    setIsAIThinking(true);
    const res = await postChat(_messages);
    const newMessage = res.data.choices[0].message;
    setMessages((prevMessages) => [...prevMessages, newMessage]);
    setIsAIThinking(false);
  }, []);

  const handleSend = useCallback(async () => {
    const newMessages = [
      ...messages,
      {
        role: "user",
        content: currentMessage + ` [Emotions: ${currentEmotions.join(", ")}]`,
      },
    ];
    setMessages(newMessages);
    postMessage(newMessages);
    setCurrentMessage("");
    setCurrentEmotions([]);
  }, [currentEmotions, currentMessage, messages, postMessage]);

  const onRecordingBlob = useCallback((blob: Blob | undefined) => {
    if (!blob) return;
    const url = URL.createObjectURL(blob);
    setAudioURL(url);
  }, []);

  return (
    <div className="bg-red-200 w-1/2 h-[400px] p-2 flex flex-col">
      OldChatbox
      <div className="w-full h-full p-2 flex flex-col gap-2 overflow-y-scroll">
        {messages.map((message) => (
          <div
            className={`shadow w-fit max-w-[80%] bg-gray-200 p-1 ${
              message.role === "assistant" ? "self-start" : "self-end"
            }`}
          >
            {message.content}
          </div>
        ))}
        {isAIThinking && (
          <div>
            <img
              className="h-20 w-20"
              src="https://media2.giphy.com/media/Sv8gUHtkqgylj9sEvS/200w.gif"
            />
          </div>
        )}
      </div>
      <div className="flex flex-col">
        <div className="flex">
          <VoiceRecorder
            onRecordingBlob={onRecordingBlob}
            disabled={isLoading}
          />
          {audioURL && (
            <>
              <audio className="h-full" src={audioURL} controls />
              <button className={`${buttonStyle}`} onClick={handleSubmit}>
                Submit audio
              </button>
            </>
          )}
        </div>
        <div className="h-[80px] flex gap-2">
          <input
            disabled={!currentMessage && currentEmotions.length === 0}
            type="text"
            value={currentMessage}
            onChange={(e) => {
              setCurrentMessage(e.target.value);
            }}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                handleSend();
              }
            }}
            className="w-full p-2"
          />
          <button
            disabled={
              isLoading || (!currentMessage && currentEmotions.length === 0)
            }
            onClick={handleSend}
            className={`${buttonStyle}`}
          >
            Submit
          </button>
        </div>
      </div>
    </div>
  );
};

export default OldChatBox;
