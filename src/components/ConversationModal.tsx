import React, { Dispatch, SetStateAction, useEffect, useState } from "react";
import { getAllDomains, getAllRoles, postConversation } from "../api";
import { IDomain, IRole } from "../types";
import Modal from "./Modal";
import FormModal from "./FormModal";
import { useNavigate } from "react-router-dom";

interface IConversationModalProps {
  showModal: boolean;
  setShowModal: Dispatch<SetStateAction<boolean>>;
}

const ConversationModal: React.FunctionComponent<IConversationModalProps> = ({
  showModal,
  setShowModal,
}) => {
  const navigate = useNavigate();
  const [companyName, setCompanyName] = useState("");
  const [companyDescription, setCompanyDescription] = useState("");
  const [domainId, setDomainId] = useState<number>(1);
  const [roleId, setRoleId] = useState<number>(1);
  const [jobDescription, setJobDescription] = useState<File | undefined>(
    undefined
  );
  const [resume, setResume] = useState<File | undefined>(undefined);
  const [message, setMessage] = useState({ error: false, content: "" });
  const [loading, setLoading] = useState(false);
  const [finished, setFinished] = useState(false);

  const [availableDomains, setAvailableDomains] = useState<IDomain[]>([]);
  const [availableRoles, setAvailableRoles] = useState<IRole[]>([]);

  useEffect(() => {
    const fetchDomainsAndRoles = async () => {
      const domainRes = await getAllDomains();
      setAvailableDomains(domainRes.data.domains);
      const roleRes = await getAllRoles();
      setAvailableRoles(roleRes.data.roles);
    };
    fetchDomainsAndRoles();
  }, []);

  const onSubmit = async (e?: React.FormEvent<HTMLFormElement>) => {
    e?.preventDefault();
    if (!resume || !jobDescription) return;
    try {
      setLoading(true);
      const formData = new FormData();
      formData.append("companyName", companyName);
      formData.append("companyDescription", companyDescription);
      formData.append("jobDescription", jobDescription);
      formData.append("domainId", domainId.toString());
      formData.append("roleId", roleId.toString());
      formData.append("resume", resume);
      await postConversation(formData);
      setFinished(true);
      setTimeout(() => {
        setFinished(false);
        setShowModal(false);
        navigate(0);
      }, 1000);
      setLoading(false);
    } catch (err) {
      console.error(err);
      setLoading(false);
    }
  };

  return (
    <>
    <FormModal
      show={showModal}
      setShow={setShowModal}
      title="Create Conversation"
      onSubmit={onSubmit}
    >
      <div className="w-full">
        <form onSubmit={onSubmit} className="w-[800px]">
          <div className="w-full mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="company-name"
            >
              Company Name
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="company-name"
              type="text"
              placeholder="ABC Limited"
              required
              onChange={(e) => {
                setCompanyName(e.target.value);
              }}
            />
          </div>
          <div className="w-full mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="job-description"
            >
              Company Description
            </label>
            <textarea
              required
              className="shadow appearance-none border rounded w-full h-[300px] py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="company-description"
              onChange={(e) => {
                setCompanyDescription(e.target.value);
              }}
            />
          </div>
          <div className="w-full mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="job-domain"
            >
              Job Domain
            </label>
            <select
              required
              value={domainId}
              onChange={(e) => setDomainId(parseInt(e.target.value))}
              name="job-domain"
            >
              {availableDomains.map((item) => (
                <option key={item.id} value={item.id}>
                  {item.label}
                </option>
              ))}
            </select>
          </div>
          <div className="w-full mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="job-role"
            >
              Job Role
            </label>
            <select
              required
              value={roleId}
              onChange={(e) => setRoleId(parseInt(e.target.value))}
              name="job-role"
            >
              {availableRoles.map((item) => (
                <option key={item.id} value={item.id}>
                  {item.label}
                </option>
              ))}
            </select>
          </div>
          <div className="w-full mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="job-description"
            >
              Job Description
            </label>
            <input
              required
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="job-description"
              type="file"
              onChange={(e) => {
                setJobDescription(e.target.files?.[0]);
              }}
            />
          </div>
          <div className="w-full mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="job-description"
            >
              Resumé
            </label>
            <input
              required
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="resume"
              type="file"
              onChange={(e) => {
                setResume(e.target.files?.[0]);
              }}
            />
          </div>
          {message.content && (
            <span
              className={`${message.error ? "text-red-500" : "text-green-500"}`}
            >
              {message.content}
            </span>
          )}
          {loading && <span>Loading...</span>}
        </form>
      </div>
    </FormModal>
    <Modal setShow={()=>{}} show={finished}>
      Request for conversation submitted. Generating questions...
    </Modal>
    </>
  );
};

export default ConversationModal;
