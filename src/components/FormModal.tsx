import { Dispatch, SetStateAction } from "react";
import { primaryButtonStyle } from "../styles";

interface IFormModalProps {
  show: boolean;
  setShow: Dispatch<SetStateAction<boolean>>;
  title: string;
  children: React.ReactNode;
  className?: string;
  onSubmit?: (() => Promise<void>) | (() => void);
}

export default function FormModal({
  show,
  setShow,
  title,
  children,
  className,
  onSubmit = () => {},
}: IFormModalProps) {
  return show ? (
    <>
      <div
        className={`justify-center items-center flex overflow-x-hidden fixed inset-0 z-50 outline-none focus:outline-none ${className}`}
      >
        <div className="relative w-auto my-6 mx-auto max-w-5xl">
          {/*content*/}
          <div
            style={{ maxHeight: "90vh" }}
            className="overflow-y-auto border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none"
          >
            {/*header*/}
            <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
              <h3 className="text-3xl font-semibold">{title}</h3>
              <button
                className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                onClick={() => setShow(false)}
              >
                <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                  ×
                </span>
              </button>
            </div>
            {/*body*/}
            <div className="relative p-6 flex-auto">{children}</div>
            {/*footer*/}
            <div className="sticky bottom-0 bg-white flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b">
              <button
                className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                type="button"
                onClick={() => setShow(false)}
              >
                Close
              </button>
              <button
                className={primaryButtonStyle}
                type="button"
                onClick={onSubmit}
              >
                Submit
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
    </>
  ) : null;
}
