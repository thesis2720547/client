import { Document, Page, Text, View, StyleSheet } from "@react-pdf/renderer";

// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: "column",
    backgroundColor: "#FFFFFF",
    paddingVertical: "20px",
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1,
  },
  title: {
    fontSize: "24px",
    fontWeight: 700,
    textAlign: "center",
    marginTop: "40px",
  },
  subtitle: {
    fontSize: "12px",
    fontWeight: 700,
    textAlign: "center",
    marginTop: "10px",
    marginBottom: "40px",
  },
  heading: {
    textAlign: "center",
    backgroundColor: "#000000",
    color: "#FFFFFF",
    paddingVertical: "5px",
    marginVertical: "8px"
  },
  recommendationHeading: {
    textAlign: "center",
    color: "#FFFFFF",
    backgroundColor: "#537fff",
    paddingVertical: "5px",
    marginVertical: "8px"
  },
  content: {
    fontSize: "12px",
    paddingVertical: "10px",
    marginBottom: "15px",
    paddingHorizontal: "40px",
  },
});

// Create Document Component
const ResultPDF = ({
  evaluation,
  conversationId
}: {
  evaluation: {
    problemSolving: string;
    candidateUnderstanding: string;
    candidateAttitude: string;
    recommendation: string;
  };
  conversationId: string;
}) => (
  <Document>
    <Page size="A4" style={styles.page}>
      <View>
        <Text style={styles.title}>Interview Report</Text>
        <Text style={styles.subtitle}>ID: {conversationId}</Text>
      </View>
      <View>
        <Text style={styles.heading}>Problem Solving</Text>
        <Text style={styles.content}>{evaluation.problemSolving}</Text>
      </View>
      <View>
        <Text style={styles.heading}>Candidate Understanding</Text>
        <Text style={styles.content}>{evaluation.candidateUnderstanding}</Text>
      </View>
      <View>
        <Text style={styles.heading}>Candidate Attitude</Text>
        <Text style={styles.content}>{evaluation.candidateAttitude}</Text>
      </View>
      <View>
        <Text style={styles.recommendationHeading}>Recommendation</Text>
        <Text style={styles.content}>{evaluation.recommendation}</Text>
      </View>
    </Page>
  </Document>
);

export default ResultPDF;
