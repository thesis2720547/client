import React, { useState } from "react";
import { buttonStyle } from "../styles";

interface IMessage {
  role: string;
  content: string;
}

interface IChatBoxProps {
  messages: IMessage[];
  onSend: () => void;
}

interface IEmotionData {
  emotions: { name: string; score: number }[];
  text: string;
}

const ChatBox: React.FunctionComponent<IChatBoxProps> = ({
  messages,
  onSend,
}) => {
  const [currentMessage, setCurrentMessage] = useState("");
  const [currentEmotions, setCurrentEmotions] = useState<string[]>([]);

  return (
    <div className="bg-red-200 w-1/2 h-[400px] p-2 flex flex-col">
      Chatbox
      <div className="w-full h-full p-2 flex flex-col gap-2 overflow-y-scroll">
        {messages.map((message) => (
          <div
            className={`shadow w-fit max-w-[80%] bg-gray-200 p-1 ${
              message.role === "assistant" ? "self-start" : "self-end"
            }`}
          >
            {message.content}
          </div>
        ))}
      </div>
      <div className="flex flex-col">
        <div className="h-[80px] flex gap-2">
          <input
            disabled={!currentMessage && currentEmotions.length === 0}
            type="text"
            value={currentMessage}
            onChange={(e) => {
              setCurrentMessage(e.target.value);
            }}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                handleSend();
              }
            }}
            className="w-full p-2"
          />
          <button
            disabled={!currentMessage && currentEmotions.length === 0}
            onClick={onSend}
            className={`${buttonStyle}`}
          >
            Submit
          </button>
        </div>
      </div>
    </div>
  );
};

export default ChatBox;
