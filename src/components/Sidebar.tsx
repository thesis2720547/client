import React, { useCallback, useEffect, useMemo, useState } from "react";
import { NavLink, useLocation } from "react-router-dom";
import { INotification, NOTIFICATION_TYPES, RESOURCE_TYPES } from "../types";
import { getAllConversations } from "../api";
import { getNotifications, markNotificationRead } from "../api/notification";
import { socketWrapper } from "../utils/socket";
import { AiOutlineClose } from "react-icons/ai";
import { useNavigate } from "react-router-dom";
import notificationSound from "../assets/notification-sound.wav";
import { logout } from "../api/auth";

const NotificationItem = ({
  notification,
}: {
  notification: INotification;
}) => {
  const navigate = useNavigate();

  const navigateToResource = useCallback(async () => {
    switch (notification.resourceType) {
      case RESOURCE_TYPES.CONVERSATION: {
        navigate(`/conversation/${notification.resourceId}`, {
          relative: "path",
        });
        break;
      }
      case RESOURCE_TYPES.QUESTION: {
        const res = await getAllConversations([
          { key: "questionId", value: notification.resourceId },
        ]);
        const conversation = res.data.conversations?.[0];
        navigate(
          `/conversation/${conversation.id}/question/${notification.resourceId}`,
          { relative: "path" }
        );
        break;
      }
    }
  }, [navigate, notification.resourceId, notification.resourceType]);

  const textContent = useMemo(() => {
    switch (notification.notificationType) {
      case NOTIFICATION_TYPES.CONVERSATION_CREATED: {
        return "Conversation created successfully.";
      }
      case NOTIFICATION_TYPES.QUESTION_ANALYSIS_READY: {
        return "Question analysis is available.";
      }
      case NOTIFICATION_TYPES.CONVERSATION_RESULT_READY: {
        return "Conversation result is ready.";
      }
    }
  }, [notification.notificationType]);

  const onClick = useCallback(async () => {
    navigateToResource();
    await markNotificationRead(notification.id);
  }, [navigateToResource, notification.id]);

  return (
    <div
      onClick={onClick}
      className={`w-full overflow-x-hidden p-3 hover:bg-gray-100 cursor-pointer ${
        notification.isRead ? "text-gray-500" : "text-black"
      }`}
    >
      <p className={`${notification.isRead ? "font-normal" : "font-[500]"}`}>
        {textContent}
      </p>{" "}
      <p className="ellipsis truncate text-sm">{notification.resourceId} </p>
    </div>
  );
};

const NotificationPopup = ({
  show,
  setShow,
}: {
  show: boolean;
  setShow: React.Dispatch<React.SetStateAction<boolean>>;
}) => {
  const [notifications, setNotifications] = useState<INotification[]>([]);
  const location = useLocation();

  useEffect(() => {
    const fetchNotifications = async () => {
      const res = await getNotifications();
      console.log(res);
      setNotifications(res.data.notifications);
    };
    fetchNotifications();
  }, [show]);

  useEffect(() => {
    setShow(false);
  }, [location, setShow]);

  return (
    <div
      onClick={() => {
        setShow(false);
      }}
      className={`z-[2] ${
        show ? "opacity-1" : "opacity-0 pointer-events-none"
      } transition-all fixed w-[250%] h-screen bg-black/30 top-0 left-0`}
    >
      <div
        className={`${
          show ? "translate-x-0" : "-translate-x-[110%]"
        } overflow-y-auto transition-all duration-600 absolute top-0 w-[300px] rounded-lg border-gray-100 shadow-xl bg-white h-screen py-4`}
      >
        <div className="flex justify-between px-4 items-center">
          <h1 className="font-bold text-xl">Notifications</h1>
          <AiOutlineClose
            className="text-2xl relative -top-1 hover:bg-gray-100 p-1 cursor-pointer"
            onClick={() => setShow(false)}
          />
        </div>
        {notifications &&
          notifications.length > 0 &&
          notifications.map((notification) => (
            <NotificationItem
              key={notification.id}
              notification={notification}
            />
          ))}
      </div>
    </div>
  );
};

export default function Sidebar() {
  const navigate = useNavigate();
  const [showNotification, setShowNotification] = useState(false);
  const [hasNewNotification, setHasNewNotification] = useState(false);
  const socket = socketWrapper.getSocket();

  useEffect(() => {
    const onNotification = () => {
      const audio = new Audio(notificationSound);
      audio.play();
      setHasNewNotification(true);
    };
    socket?.on("notification", onNotification);

    return () => {
      socket?.off("notification", onNotification);
    };
  }, [socket]);

  const handleLogout = useCallback(async () => {
    await logout();
    navigate("/auth/login");
  }, []);

  return (
    <div className="flex flex-col h-scren p-3 bg-black shadow w-[60px] relative">
      <div className="space-y-3">
        <div className="flex items-center"></div>
        <div className="flex-1">
          <ul className="pt-2 pb-4 space-y-1 text-sm">
            <li className="rounded-sm">
              <NavLink
                to="/"
                className={({ isActive, isPending }) =>
                  `flex items-center p-2 space-x-3 rounded-md ${
                    isPending
                      ? "pending"
                      : isActive
                      ? "text-white"
                      : "text-gray-500"
                  }`
                }
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-6 h-6"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
                  />
                </svg>
              </NavLink>
            </li>
            <li className="rounded-sm">
              <div
                className="flex items-center p-2 space-x-3 rounded-md relative cursor-pointer"
                onClick={() => {
                  setShowNotification((prev) => {
                    if (!prev) setHasNewNotification(false);
                    return !prev;
                  });
                }}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-6 h-6 text-gray-500 hover:text-white"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M20 13V6a2 2 0 00-2-2H6a2 2 0 00-2 2v7m16 0v5a2 2 0 01-2 2H6a2 2 0 01-2-2v-5m16 0h-2.586a1 1 0 00-.707.293l-2.414 2.414a1 1 0 01-.707.293h-3.172a1 1 0 01-.707-.293l-2.414-2.414A1 1 0 006.586 13H4"
                  />
                </svg>
                {hasNewNotification && (
                  <svg
                    id="red-dot"
                    xmlns="http://www.w3.org/2000/svg"
                    className="absolute top-0 right-0 text-gray-500 hover:text-white"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth={2}
                  >
                    <circle
                      cx="16"
                      cy="10"
                      r="4"
                      stroke="transparent"
                      stroke-width="3"
                      fill="red"
                    />
                  </svg>
                )}
              </div>
            </li>
            <li className="rounded-sm">
              <div
                className="flex items-center p-2 space-x-3 rounded-md"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-6 h-6 text-gray-500 hover:text-white"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"
                  />
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"
                  />
                </svg>
              </div>
            </li>
            <li className="rounded-sm">
              <button
                onClick={handleLogout}
                className="flex items-center p-2 space-x-3 rounded-md"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-6 h-6 text-gray-500 hover:text-white"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  strokeWidth={2}
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M11 16l-4-4m0 0l4-4m-4 4h14m-5 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h7a3 3 0 013 3v1"
                  />
                </svg>
              </button>
            </li>
          </ul>
        </div>
      </div>
      <NotificationPopup
        show={showNotification}
        setShow={setShowNotification}
      />
    </div>
  );
}
