import { Dispatch, SetStateAction } from "react";
import { AiOutlineClose } from "react-icons/ai";

interface IModalProps {
  show: boolean;
  setShow?: Dispatch<SetStateAction<boolean>>;
  children: React.ReactNode;
  className?: string;
}

export default function Modal({
  show,
  setShow,
  children,
  className,
}: IModalProps) {
  return show ? (
    <>
      <div className="justify-center items-center flex overflow-x-hidden fixed inset-0 z-50 outline-none focus:outline-none ${className}">
        <div className={`relative w-auto my-6 mx-auto ${className}`}>
          {/*content*/}
          <div
            style={{ maxHeight: "90vh" }}
            className="overflow-y-auto border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none"
          >
            {setShow && (
              <button>
                <AiOutlineClose
                  className="text-3xl absolute right-3 top-2 hover:bg-gray-100 p-1 cursor-pointer z-[10]"
                  onClick={() => setShow(false)}
                />
              </button>
            )}

            <div className="relative p-6 flex-auto">{children}</div>
          </div>
        </div>
      </div>
      <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
    </>
  ) : null;
}
