import axios from "axios";
import { IDomain, IRole } from "../types";
axios.defaults.withCredentials = true;

const baseURL = `${import.meta.env.VITE_SERVER_URL}/v1`;

export const postChat = async (
  messages: { role: string; content: string }[]
) => {
  const res = await axios.post(`${baseURL}/chat`, messages);
  return res;
};

export const postAudio = async (formData: FormData) => {
  const res = await axios.post(`${baseURL}/audio`, formData);
  return res;
};

export const postVideo = async (formData: FormData) => {
  const res = await axios.post(`${baseURL}/video`, formData);
  return res;
};

export const postConversation = async (formData: FormData) => {
  const res = await axios.post(`${baseURL}/conversation`, formData);
  return res;
};

export const getAllConversations = async (
  queries?: { key: string; value: string }[]
) => {
  const res = await axios.get(
    `${baseURL}/conversation?` +
      queries?.map((query) => `${query.key}=${query.value}`).join("&")
  );
  return res;
};

export const getConversation = async (convoId: string) => {
  const res = await axios.get(`${baseURL}/conversation/${convoId}`);
  return res;
};

export const getConversationEvaluation = async (convoId: string) => {
  const res = await axios.get(`${baseURL}/conversation/evaluation/${convoId}`);
  return res;
};

export const getAllQuestions = async (convoId: string) => {
  const res = await axios.get(
    `${baseURL}/conversation/question?convoId=${convoId}`
  );
  return res;
};

export const getQuestion = async (questionId: string) => {
  const res = await axios.get(`${baseURL}/conversation/question/${questionId}`);
  return res;
};

export const putAnswerContent = async (
  questionId: string,
  answerContent: string
) => {
  const res = await axios.put(
    `${baseURL}/conversation/question/${questionId}`,
    { answer: answerContent }
  );
  return res;
};

export const postAnswer = async (questionId: string, formData: FormData) => {
  const res = await axios.post(
    `${baseURL}/conversation/question/${questionId}/answer`,
    formData
  );
  return res;
};

export const getMediaUrl = async (objectKey: string) => {
  const res = await axios.get(
    `${baseURL}/conversation/media?objectKey=${objectKey}`
  );
  console.log(res);
  return res;
};

export const getAllDomains = async () => {
  const res = await axios.get<{ domains: IDomain[]; total: number }>(
    `${baseURL}/domain`
  );
  return res;
};

export const getAllRoles = async () => {
  const res = await axios.get<{ roles: IRole[]; total: number }>(
    `${baseURL}/role`
  );
  return res;
};
