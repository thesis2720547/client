import axios from "axios";
import { INotification } from "../types";
axios.defaults.withCredentials = true;

const baseURL = `${import.meta.env.VITE_SERVER_URL}/v1`;

export const getNotifications = async () => {
  const res = await axios.get<{
    notifications: INotification[];
    total: number;
  }>(`${baseURL}/notification`);
  return res;
};

export const markNotificationRead = async (notificationId: string) => {
  const res = await axios.put(`${baseURL}/notification/${notificationId}`);
  return res;
};
