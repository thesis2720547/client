import axios from "axios";
axios.defaults.withCredentials = true;

const baseURL = `${import.meta.env.VITE_SERVER_URL}/v1`;

export const login = async ({
  email,
  password,
}: {
  email: string;
  password: string;
}) => {
  const res = await axios.post(`${baseURL}/auth/login`, { email, password });
  return res;
};

export const register = async ({
  name,
  email,
  password,
}: {
  name: string;
  email: string;
  password: string;
}) => {
  const res = await axios.post(`${baseURL}/auth/register`, { name, email, password });
  return res;
};

export const logout = async () => {
  const res = await axios.post(`${baseURL}/auth/logout`);
  return res;
};

export const getAuth = async () => {
  const res = await axios.get(`${baseURL}/auth`);
  return res;
};

export const postConversation = async (formData: FormData) => {
  const res = await axios.post(`${baseURL}/conversation`, formData);
  return res;
};
