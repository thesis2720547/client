export interface IDomain {
  id: number;
  name: string;
  label: string;
  description: string;
}

export interface IRole {
  id: number;
  name: string;
  label: string;
  description: string;
}

export enum RESOURCE_TYPES {
  QUESTION = "Question",
  CONVERSATION = "Conversation",
}

export enum NOTIFICATION_TYPES {
  CONVERSATION_CREATED = "conversation_created",
  QUESTION_ANALYSIS_READY = "question_analysis_ready",
  CONVERSATION_RESULT_READY = "conversation_result_ready",
}

export interface INotification {
  id: string;
  userId: string;
  notificationType: NOTIFICATION_TYPES;
  resourceType: RESOURCE_TYPES;
  resourceId: string;
  isRead: boolean;
}
