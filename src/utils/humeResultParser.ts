export const parseSpeechProsody = (resultString: string) => {
  if (!resultString) return null;
  try {
    const result = JSON.parse(resultString);
    const models = result[0].models;
    const prosody = models.prosody;
    const prosodyEmotions =
      prosody.grouped_predictions[0].predictions[0].emotions;
    const topEmotions = prosodyEmotions.sort(
      (emotion1: any, emotion2: any) => emotion2.score - emotion1.score
    );
    const transcription = prosody.grouped_predictions[0].predictions[0].text;

    return {
      emotions: topEmotions.slice(0, 3),
      text: transcription,
    };
  } catch (err) {
    console.error(err);
    return null;
  }
};
