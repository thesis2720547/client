import { Socket, io } from "socket.io-client";

// "undefined" means the URL will be computed from the `window.location` object
// const URL =
//   process.env.NODE_ENV === "production" ? undefined : "http://localhost:4000";

const URL = import.meta.env.VITE_SERVER_URL;

class SocketWrapper {
  private socket: Socket | null = null;

  public initSocket = (userId: string) => {
    if (this.socket) return;
    this.socket = io(URL, {
      auth: {
        userId: userId,
      },
    });
  };

  public getSocket = () => {
    return this.socket;
  };
}

const socketWrapper = new SocketWrapper();

export { socketWrapper };
