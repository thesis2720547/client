export const buttonStyle =
  "bg-white cursor-pointer rounded border py-1 px-2 enabled:hover:bg-gray-100 disabled:text-gray-400 disabled:bg-gray-200 disabled:cursor-default";

  export const primaryButtonStyle =
  "bg-gray-700 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150";
