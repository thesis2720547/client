FROM node:18-alpine

RUN npm i -g pnpm

WORKDIR /app

COPY package.json .
RUN pnpm i

COPY . .

## EXPOSE [Port you mentioned in the vite.config file]

EXPOSE 5173

CMD ["pnpm", "run", "dev"]